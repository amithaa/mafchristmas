using UnityEngine;
using System.Collections;
using ArabicSupport;
using UnityEngine.UI;

public class Fix3dTextCS : MonoBehaviour {

	public void FitTextDynamic () {	

		string text = gameObject.GetComponent<InputField> ().text;
		gameObject.GetComponent<InputField>().text = ArabicFixer.Fix(text, false, false);
	}
}
