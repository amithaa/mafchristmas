using UnityEngine;
using System.Collections;
using ArabicSupport;
using UnityEngine.UI;


public class SetArabicTextExample : MonoBehaviour {
	
	
	// Use this for initialization
	void Start () {	

		string text = gameObject.GetComponent<Text> ().text;
		gameObject.GetComponent<Text>().text = ArabicFixer.Fix(text, false, false);
	}


	void FitTextDynamic () {	

		string text = gameObject.GetComponent<InputField> ().text;
		gameObject.GetComponent<InputField>().text = ArabicFixer.Fix(text, false, false);
	}

}
