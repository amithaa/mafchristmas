﻿using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI;
using System.Collections.Generic;
using ArabicSupport;

public class FacebookScript : MonoBehaviour
{

	//The token returned
	public string userID = "";
	//Access token returned
	AccessToken token;
	//Name
	public string firstName = "";
	//Lastname
	public string lastName = "";
	//email
	public string email = "";
	//gender
	public string gender = "";

	public string shareText;

	void Start()
	{
//		shareText = ArabicFixer.Fix(shareText, false, false);
	}


	void Awake ()
	{
		FB.Init (SetInit, OnHideUnity);
	}

	//Initialise
	void SetInit ()
	{

		if (FB.IsLoggedIn) {
			Debug.Log ("FB is logged in");
		} else {
			Debug.Log ("FB is not logged in");
		}

	}

	//On hiding
	void OnHideUnity (bool isGameShown)
	{

		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}

	}

	//Login to Facebook
	public void FBlogin ()
	{
		FB.LogInWithReadPermissions (new List<string> () { "public_profile, email" }, AuthCallBack);
	}


	//Log out of Facebook
	public void FBLogout ()
	{
		FB.LogOut ();
		if (FB.IsLoggedIn) {
			Debug.Log ("Still Logged in");
		} else {
			Debug.Log ("Successfully logged out");
		}
	}


	//Call Back on logging in
	void AuthCallBack (IResult result)
	{

		if (result.Error != null) {
//			Debug.Log (result.Error);
			UIPanelManager.Instance.AlertError (result.Error);
		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB is logged in");
				foreach (string perm in Facebook.Unity.AccessToken.CurrentAccessToken.Permissions)
					Debug.Log (perm);
				FB.API ("/me?fields=first_name,last_name,email,gender", HttpMethod.GET, DisplayUsername, new Dictionary<string, string> () { });

			} else {
				Debug.Log ("FB is not logged in");
			}

		}

	}


	//Fetch the username
	void DisplayUsername (IResult result)
	{

		if (result.Error == null) {

			try {
				firstName = result.ResultDictionary ["first_name"].ToString ();
			} catch (System.Exception e) {
				print (e);
				firstName = "";
			}
			try {
				lastName = result.ResultDictionary ["last_name"].ToString ();
			} catch (System.Exception e) {
				print (e);
				lastName = "";
			}
			try {
				email = result.ResultDictionary ["email"].ToString ();
			} catch (System.Exception e) {
				print (e);
				email = "";
			}
			try {
				gender = result.ResultDictionary ["gender"].ToString ();
			} catch (System.Exception e) {
				print (e);
				gender = "";
			}
			token = AccessToken.CurrentAccessToken;
			userID = token.UserId;



		} else {
			Debug.Log (result.Error);
			UIPanelManager.Instance.AlertError (result.Error);
		}

		if (UIPanelManager.Instance.language == "English") {
			ProcessForm.Instance.SetInputFieldsEnglish (firstName, lastName, email);
			UIPanelManager.Instance.OpenFormPanel ();

		} else {
			ProcessForm.Instance.SetInputFieldsArabic (firstName, lastName, email);
			UIPanelManager.Instance.OpenFormPanelArabic ();
		}

	}


	//Post on Facebook wall
	public void SharePost ()
	{
		if (UIPanelManager.Instance.provider == "Facebook") {
			FB.LogInWithPublishPermissions (new List<string> () { "publish_actions" }, ShareCallBack);
		}

	}

	//Call Back on logging in
	void ShareCallBack (IResult result)
	{

		if (result.Error != null) {
			Debug.Log (result.Error);
			UIPanelManager.Instance.AlertError (result.Error);

		} else {
				
			if (FB.IsLoggedIn) {
				var wwwForm = new WWWForm ();
				wwwForm.AddBinaryData ("image", WebCamTexturePlay.Instance.bytes, WebCamTexturePlay.Instance.storedPhotoPath);
				wwwForm.AddField ("message", shareText);

				FB.API ("me/photos", HttpMethod.POST, APICallback, wwwForm);
				SubmitToDatabase ();
			}
		}

	}



	//Call back on sharing
	public void APICallback (IResult result)
	{
		if (result.Error != null) {
			Debug.Log (result.Error);
			UIPanelManager.Instance.AlertError (result.Error);

		}
	}


	void OnApplicationQuit ()
	{
		FBLogout ();
	}

	public void SubmitToDatabase ()
	{

		if (UIPanelManager.Instance.provider == "Facebook" && UIPanelManager.Instance.language == "English") {
			string gender = (ProcessForm.Instance.title == "Mr") ? "male" : "female";  
			DatabaseConnection.Instance.userID = DatabaseConnection.Instance.imageHTTPRequestValidation (ProcessForm.Instance.title, gender, firstName, lastName, ProcessForm.Instance.email, ProcessForm.Instance.phone, token.TokenString, userID, UIPanelManager.Instance.provider, UIPanelManager.Instance.language, WebCamTexturePlay.Instance.fileName, WebCamTexturePlay.Instance.fileName, UIPanelManager.Instance.participate);
		} else if (UIPanelManager.Instance.provider == "Facebook" && UIPanelManager.Instance.language == "Arabic") {
			string gender = (ProcessForm.Instance.titleArabic == "Mr") ? "male" : "female"; 
			DatabaseConnection.Instance.userID = DatabaseConnection.Instance.imageHTTPRequestValidation (ProcessForm.Instance.titleArabic, gender, firstName, lastName, ProcessForm.Instance.emailArabic, ProcessForm.Instance.phoneArabic, token.TokenString, userID, UIPanelManager.Instance.provider, UIPanelManager.Instance.language, WebCamTexturePlay.Instance.fileName, WebCamTexturePlay.Instance.fileName, UIPanelManager.Instance.participate);
		}
	}

}