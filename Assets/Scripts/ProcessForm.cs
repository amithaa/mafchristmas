﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;


public class ProcessForm : MonoBehaviour
{
	private static ProcessForm _instance;

	public static ProcessForm Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	//UI
	public InputField firstNameText;
	public InputField lastNameText;
	public InputField emailText;
	public InputField phoneText;
	public Dropdown titleText;

	public InputField firstNameTextArabic;
	public InputField lastNameTextArabic;
	public InputField emailTextArabic;
	public InputField phoneTextArabic;
	public Dropdown titleTextArabic;

	//Strings
	public string firstName;
	public string lastName;
	public string email;
	public string phone;
	public string title;


	public string firstNameArabic;
	public string lastNameArabic;
	public string emailArabic;
	public string phoneArabic;
	public string titleArabic;
	//T&C
	public bool TandC = false;
	public bool TandCArabic = false;


	void Start()
	{
		title = titleText.options [0].text;
		titleArabic = titleTextArabic.options [0].text;
	}

	//Submit to database
	public void SubmitForm ()
	{
		if (TandC || TandCArabic) {
			if (UIPanelManager.Instance.language == "English") {
				UIPanelManager.Instance.setUserNameText (firstName);
				UIPanelManager.Instance.OpenChainPanel ();
			} else {
				UIPanelManager.Instance.setUserNameTextArabic (firstNameArabic);
				UIPanelManager.Instance.OpenFChainPanelArabic ();
			}
		}
	}


	public void SetInputFieldsEnglish(string _firstname, string _lastname, string _email)
	{
		firstNameText.text = _firstname;
		lastNameText.text = _lastname;
		emailText.text = _email;

		firstName = _firstname;
		lastName = _lastname;
	}

	public void SetInputFieldsArabic(string _firstname, string _lastname, string _email)
	{
		firstNameTextArabic.text = _firstname;
		lastNameTextArabic.text = _lastname;
		emailTextArabic.text = _email;

		firstNameArabic = _firstname;
		lastNameArabic = _lastname;
	}

	//Add first name
	public void FirstNameTextChanged ()
	{
		firstName = firstNameText.text.ToString ();
	}

	//Add last name
	public void LastNameTextChanged ()
	{
		lastName = lastNameText.text.ToString ();
	}

	//Add email
	public void EmailTextChanged ()
	{
		email = emailText.text.ToString ();
	}

	//Add phone
	public void PhoneTextChanged ()
	{
		phone = phoneText.text.ToString ();
	}

	//Add title
	public void TitleTextChanged (int index)
	{
		title = titleText.options [index].text;
	}

	//Accept Terms and conditions
	public void ToggleTandC (bool newValue)
	{
		TandC = newValue;
	}


	//Add first name
	public void FirstNameTextChangedArabic ()
	{
//		firstNameTextArabic.text = ArabicFixer.Fix(firstNameTextArabic.text, false, false);
		firstNameArabic = firstNameTextArabic.text.ToString ();

	}

	//Add last name
	public void LastNameTextChangedArabic ()
	{
		lastNameArabic = lastNameTextArabic.text.ToString ();
	}

	//Add email
	public void EmailTextChangedArabic ()
	{
		emailArabic = emailTextArabic.text.ToString ();
	}

	//Add phone
	public void PhoneTextChangedArabic ()
	{
		phoneArabic = phoneTextArabic.text.ToString ();
	}

	//Add title
	public void TitleTextChangedArabic (int index)
	{
		titleArabic = titleTextArabic.options [index].text;
	}

	//Accept Terms and conditions
	public void SetTandCAndSubmit()
	{
		TandC = true;
		SubmitForm ();
	}



	//Accept Terms and conditions
	public void SetTandCArabicAndSubmit()
	{
		TandCArabic = true;
		SubmitForm ();
	}

	public void ResetForm()
	{
		firstNameText.text = "";
		lastNameText.text = "";
		emailText.text = "";
		phoneText.text = "";
		TandC = false;
		firstNameTextArabic.text = "";
		lastNameTextArabic.text = "";
		emailTextArabic.text = "";
		phoneTextArabic.text = "";
		TandCArabic= false;
	}

	public void SubmitToDatabase()
	{
		if (UIPanelManager.Instance.provider == "Email" && UIPanelManager.Instance.language == "English") {
			string gender = (title == "Mr") ? "male" : "female";  
			DatabaseConnection.Instance.userID = DatabaseConnection.Instance.imageHTTPRequestValidation (title, gender, firstName, lastName, email, phone, "", "", UIPanelManager.Instance.provider, UIPanelManager.Instance.language, WebCamTexturePlay.Instance.fileName, WebCamTexturePlay.Instance.fileName, UIPanelManager.Instance.participate);
		} else if (UIPanelManager.Instance.provider == "Email" && UIPanelManager.Instance.language == "Arabic") {
			string gender = (titleArabic == "Mr") ? "male" : "female";  
			DatabaseConnection.Instance.userID = DatabaseConnection.Instance.imageHTTPRequestValidation (titleArabic, gender, firstNameArabic, lastNameArabic, emailArabic, phoneArabic, "", "", UIPanelManager.Instance.provider, UIPanelManager.Instance.language, WebCamTexturePlay.Instance.fileName, WebCamTexturePlay.Instance.fileName, UIPanelManager.Instance.participate);
		}
	}
}
