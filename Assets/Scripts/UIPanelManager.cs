﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;


public class UIPanelManager : MonoBehaviour
{

	private static UIPanelManager _instance;

	public static UIPanelManager Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	public GameObject logoPanel;
	public GameObject languagePanel;
	public GameObject registerPanel;
	public GameObject thankYouPanel;
	public GameObject cameraPanel;
	public GameObject formPanel;
	public GameObject hiddenPanel;
	public GameObject reviewPanel;
	public GameObject chainPanel;
	public GameObject registerPanelArabic;
	public GameObject formPanelArabic;
	public GameObject chainPanelArabic;
	public GameObject cameraPanelArabic;
	public GameObject reviewPanelArabic;
	public GameObject thankYouPanelArabic;
	public Text debugText;


	public Text userNameText;
	public Text userNameTextArabic;

	public string language = "English";
	public string provider = "Facebook";
	public int participate = 0;

	//Open Logo Panel
	public void OpenLogoPanel ()
	{
		logoPanel.SetActive (true);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);


	}


	//Open Language Panel
	public void OpenLanguagePanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (true);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}

	//Open Register Panel
	public void OpenRegisterPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		thankYouPanel.SetActive (false);
		registerPanel.SetActive (true);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}

	//Open Thank you Panel
	public void OpenThankYouPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (true);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

		SayThanks ();

	}

	//Open Camera Panel
	public void OpenCameraPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (true);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

		WebCamTexturePlay.Instance.StartCamera ();

	}

	//Open Form Panel
	public void OpenFormPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (true);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}
		

	//Open Review Panel
	public void OpenReviewPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (true);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

		EditAndReviewPhoto.Instance.LoadPhoto ();

	}

	//Open Chain panel
	public void OpenChainPanel ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (true);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}


	//Open Register Panel Arabic
	public void OpenRegisterPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		thankYouPanel.SetActive (false);
		registerPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		registerPanelArabic.SetActive (true);
		formPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}

	//Open Form Panel
	public void OpenFormPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (true);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}

	//Open Form Panel
	public void OpenFChainPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (true);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

	}


	//Open Form Panel
	public void OpenCameraPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (true);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (false);

		WebCamTexturePlay.Instance.StartCamera ();

	}

	//Open Form Panel
	public void OpenReviewPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (true);
		thankYouPanelArabic.SetActive (false);

		EditAndReviewPhoto.Instance.LoadPhoto ();

	}


	//Open Form Panel
	public void OpenThankYouPanelArabic ()
	{
		logoPanel.SetActive (false);
		languagePanel.SetActive (false);
		registerPanel.SetActive (false);
		thankYouPanel.SetActive (false);
		cameraPanel.SetActive (false);
		formPanel.SetActive (false);
		reviewPanel.SetActive (false);
		chainPanel.SetActive (false);
		formPanelArabic.SetActive (false);
		registerPanelArabic.SetActive (false);
		chainPanelArabic.SetActive (false);
		cameraPanelArabic.SetActive (false);
		reviewPanelArabic.SetActive (false);
		thankYouPanelArabic.SetActive (true);

		SayThanks ();
	}


	//Thank you screen
	public void SayThanks ()
	{
		StartCoroutine (DisableThankYouPanel ());
	}

	public void SetProvider()
	{
		provider = "Email";
	}


	//Fetch username and disable thank you
	IEnumerator DisableThankYouPanel ()
	{
		yield return new WaitForSeconds (3.0f);

		//Reset 
		ResetAll ();

		OpenLogoPanel ();
	}


	//Set the user name in the thank you field
	public void setUserNameText (string userName)
	{
		userNameText.text = userName;
	}


	//Set the user name in the thank you field
	public void setUserNameTextArabic (string userName)
	{
		userNameTextArabic.text = userName;
		userNameTextArabic.text = ArabicFixer.Fix(userNameTextArabic.text, false, false);
	}


	public void SetParticipate()
	{
		participate = 1;
	}

	//Reset all and wait for next user
	public void ResetAll ()
	{
		userNameText.text = "";
		participate = 0;
		language = "English";
		provider = "Facebook";
		WebCamTexturePlay.Instance.SetAll();
		ProcessForm.Instance.ResetForm ();
		EditAndReviewPhoto.Instance.ResetAll ();
	}

	public void SetLanguageToArabic()
	{
		language = "Arabic";
		WebCamTexturePlay.Instance.SetArabic ();
		EditAndReviewPhoto.Instance.setDisplayPanelArabic ();
	}

	public void AlertError(string errorText)
	{
		debugText.gameObject.SetActive (true);
		debugText.text = errorText;
		StartCoroutine (DisableErrorText());
	}

	IEnumerator DisableErrorText()
	{
		yield return new WaitForSeconds (10.0f);
		debugText.text = "";
		debugText.gameObject.SetActive (false);

	}
}
