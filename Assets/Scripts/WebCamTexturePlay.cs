﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;


public class WebCamTexturePlay : MonoBehaviour
{
	private static WebCamTexturePlay _instance;

	public static WebCamTexturePlay Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	WebCamTexture webCamTexture;
	public RawImage finalRawimage;
	private bool camAvailable;
	public AspectRatioFitter finalFit;
	public RawImage rawimageArabic;
	public AspectRatioFitter fitArabic;
	public RawImage rawimageEnglish;
	public AspectRatioFitter fitEnglish;

	public string storedPhotoPath;
	string path;


	public Text debugText;
	public GameObject frameFinal;
	public GameObject clickButtonFinal;
	public GameObject overlayImageFinal;

	public GameObject frameFinalEnglish;
	public GameObject clickButtonFinalEnglish;
	public GameObject overlayImageFinalEnglish;

	public GameObject frameFinalArabic;
	public GameObject clickButtonFinalArabic;
	public GameObject overlayImageFinalArabic;


	public byte[] bytes;

	public string fileName;

	void Start ()
	{
		SetAll ();
		path = Application.persistentDataPath + "\\";

		//Checking for cameras
		WebCamDevice[] devices = WebCamTexture.devices;

		if (devices.Length == 0) {
			Debug.Log ("No camera detected");
			UIPanelManager.Instance.AlertError ("No camera detected");

			camAvailable = false;
			return;
		}

		for (int i = 0; i < devices.Length; ++i) {
			if (!devices [i].isFrontFacing) {
				webCamTexture = new WebCamTexture (devices [i].name, Screen.width, Screen.height);

			}

			if (webCamTexture == null) {
				Debug.Log ("No back cam");
				UIPanelManager.Instance.AlertError ("No camera detected");

				return;
			}
		}
			
	}



	public void SetAll ()
	{
		finalRawimage = rawimageEnglish;
		finalFit = fitEnglish;
		frameFinal = frameFinalEnglish;
		clickButtonFinal = clickButtonFinalEnglish;
		overlayImageFinal = overlayImageFinalEnglish;
	}

	public void SetArabic ()
	{
		finalRawimage = rawimageArabic;
		finalFit = fitArabic;
		frameFinal = frameFinalArabic;
		clickButtonFinal = clickButtonFinalArabic;
		overlayImageFinal = overlayImageFinalArabic;
	}

	void Update ()
	{
		//Adjust the camera on the device
		if (!camAvailable)
			return;
		float ratio = (float)webCamTexture.width / (float)webCamTexture.height;
		finalFit.aspectRatio = ratio;

		float scaleY = webCamTexture.videoVerticallyMirrored ? -1f : 1f;
		finalRawimage.rectTransform.localScale = new Vector3 (1f, scaleY, 1f);

		int orient = -webCamTexture.videoRotationAngle;
		finalRawimage.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);
	}


	//Start the camera
	public void StartCamera ()
	{
		camAvailable = true;
		//Set the raw texture to camera
		finalRawimage.texture = webCamTexture;

		webCamTexture.Play ();
	}


	//Stop the camera
	public void StopCamera ()
	{
		camAvailable = true;
		webCamTexture.Stop ();
	}


	//Capture the screenshot
	public void CapturePhoto ()
	{
		StartCoroutine (TakePhoto ());
	}


	private Texture2D ScaleTexture (Texture2D source, int targetWidth, int targetHeight)
	{
		Texture2D result = new Texture2D (targetWidth, targetHeight, source.format, true);
		Color[] rpixels = result.GetPixels (0);
		float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
		float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);
		for (int px = 0; px < rpixels.Length; px++) {
			rpixels [px] = source.GetPixelBilinear (incX * ((float)px % targetWidth),
				incY * ((float)Mathf.Floor (px / targetWidth)));
		}
		result.SetPixels (rpixels, 0);
		result.Apply ();
		return result;
	}


	//Capture the screenshot
	IEnumerator TakePhoto ()
	{
		frameFinal.SetActive (false);
		clickButtonFinal.SetActive (false);
		overlayImageFinal.SetActive (true);

		yield return new WaitForEndOfFrame (); 
		try {
			fileName = System.DateTime.Now.ToString ("yyyyMMddHHmmssfff") + ".jpg";
			storedPhotoPath = path + fileName;

//			Texture2D photo = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);

			Texture2D photo = new Texture2D (1080, 1440, TextureFormat.RGB24, false);

//			photo.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);

			photo.ReadPixels (new Rect (0, 480, 1080, 1440f), 0, 0);
			photo.Apply ();

			Texture2D newScreenshot = ScaleTexture (photo, 768, 1024);

			//Encode to a PNG/JPG
			bytes = newScreenshot.EncodeToJPG ();


			File.WriteAllBytes (storedPhotoPath, bytes);

		} catch (System.Exception e) {
			print (e);
			UIPanelManager.Instance.AlertError (e.ToString());

			storedPhotoPath = "";
			fileName = "";
		}


		frameFinal.SetActive (true);
		clickButtonFinal.SetActive (true);
		overlayImageFinal.SetActive (false);

		if (UIPanelManager.Instance.language == "English")
			UIPanelManager.Instance.OpenReviewPanel ();
		else
			UIPanelManager.Instance.OpenReviewPanelArabic ();
	}
}