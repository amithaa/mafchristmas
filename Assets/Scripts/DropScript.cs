﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropScript : MonoBehaviour, IDropHandler {

	
	#region IDropHandler implementation

	public void OnDrop (PointerEventData eventData)
	{
		DragHandler.itemBeingDragged.transform.SetParent (transform);
			
	}

	#endregion



}
