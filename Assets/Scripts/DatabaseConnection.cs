﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;


public class DatabaseConnection : MonoBehaviour
{
	private static DatabaseConnection _instance;

	public static DatabaseConnection Instance { get { return _instance; } }


	public string sender = "amitha@pixelplusmedia.com";
	public string receiver = "amita92@gmail.com";
	public string smtpPassword = "Amithaarun10";
	public string smtpHost = "mail.pixelplusmedia.com";

	public string userID;

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}
		

	// Class to call api
	public string imageHTTPRequestValidation (string _title, string _gender, string _firstname, string _lastname, string _email, string _mobile, string _access_token, string _user_id, 
	                                          string _provider, string _language, string _image_name_large, string _image_name_thumbs, int _participate)
	{

		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("http://192.168.1.13:3000/api/v1.0/saveuserdetails");
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";

			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"title\":\"" + _title + "\",\"gender\":\"" + _gender + "\",\"first_name\":\"" + _firstname + "\",\"last_name\":\"" + _lastname + "\",\"email\":\"" + _email + "\",\"mobile\":\"" + _mobile + "\",\"fbtoken\":\"" + _access_token + "\",\"userid\":\"" + _user_id + "\",\"language\":\"" + _language + "\",\"img_large\":\"" + _image_name_large + "\",\"img_small\":\"" + _image_name_thumbs + "\",\"provider\":\"" + _provider + "\",\"participate\":\"" + _participate + "\"}";

				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();

			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();

				return data.ToString ();
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e);
			UIPanelManager.Instance.AlertError (e.ToString());

			return "";
		}
	}


	// Class to call api
	public void SendBaubleUp ()
	{
		print ("Pressed");
		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("http://192.168.1.82:3000/up");
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "GET";


			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();

//				return data.ToString ();
			}

		} catch (System.Exception e) {
			// Your catch here
			print (e.ToString());
			UIPanelManager.Instance.AlertError (e.ToString());

//			return "";
		}
	}



	public void HttpUploadFile ()
	{
		string url = "http://192.168.1.13:3000/api/v1.0/upload_image";
		string file = WebCamTexturePlay.Instance.storedPhotoPath;
		string paramName = "file";
		string contentType = "image/jpg";
		NameValueCollection nvc = new NameValueCollection ();
		nvc.Add ("id", "TTR");
		nvc.Add ("btn-submit-photo", "Upload");


		string boundary = "---------------------------" + System.DateTime.Now.Ticks.ToString ("x");
		byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes ("\r\n--" + boundary + "\r\n");

		HttpWebRequest wr = (HttpWebRequest)WebRequest.Create (url);
		wr.ContentType = "multipart/form-data; boundary=" + boundary;
		wr.Method = "POST";
		wr.KeepAlive = true;
		wr.Credentials = System.Net.CredentialCache.DefaultCredentials;

		Stream rs = wr.GetRequestStream ();

		string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
		foreach (string key in nvc.Keys) {
			rs.Write (boundarybytes, 0, boundarybytes.Length);
			string formitem = string.Format (formdataTemplate, key, nvc [key]);
			byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes (formitem);
			rs.Write (formitembytes, 0, formitembytes.Length);
		}
		rs.Write (boundarybytes, 0, boundarybytes.Length);

		string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
		string header = string.Format (headerTemplate, paramName, file, contentType);
		byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes (header);
		rs.Write (headerbytes, 0, headerbytes.Length);

		FileStream fileStream = new FileStream (file, FileMode.Open, FileAccess.Read);
		byte[] buffer = new byte[4096];
		int bytesRead = 0;
		while ((bytesRead = fileStream.Read (buffer, 0, buffer.Length)) != 0) {
			rs.Write (buffer, 0, bytesRead);
		}
		fileStream.Close ();

		byte[] trailer = System.Text.Encoding.ASCII.GetBytes ("\r\n--" + boundary + "--\r\n");
		rs.Write (trailer, 0, trailer.Length);
		rs.Close ();

		WebResponse wresp = null;
		try {
			wresp = wr.GetResponse ();
			Stream stream2 = wresp.GetResponseStream ();
			StreamReader reader2 = new StreamReader (stream2);
			print (string.Format ("File uploaded, server response is: {0}", reader2.ReadToEnd ()));
//			SendEmail(userID,"en");
		} catch (System.Exception ex) {
			print (ex.ToString());
			UIPanelManager.Instance.AlertError (ex.ToString());

			if (wresp != null) {
				wresp.Close ();
				wresp = null;
			}
		} finally {
			wr = null;
		}
	}


	//	public void SendEmail (string _userid, string _language)
	//	{
	//
	//		try {
	//			var httpWebRequest = (HttpWebRequest)WebRequest.Create ("http://192.168.1.21:3000/api/v1.0/sendmail");
	//			httpWebRequest.ContentType = "application/json";
	//			//			httpWebRequest.Headers.Add ("Authorization:token f34e190a0fad8882e727dcf1c0da922b");
	//			httpWebRequest.Method = "POST";
	//
	//			using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
	//				string json = "{\"userid\":\"" + _userid + "\",\"language\":\"" + _language + "\"}";
	//
	//				streamWriter.Write (json);
	//				streamWriter.Flush ();
	//				streamWriter.Close ();
	//
	//			}
	//
	//			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
	//			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
	//				var data = streamReader.ReadToEnd ();
	//				print (data);
	//
	//			}
	//
	//		} catch (System.Exception e) {
	//			// Your catch here
	//			print (e);
	//			//			return string.Empty;
	//		}
	//	}



}
