﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public static GameObject itemBeingDragged;
	GameObject duplicateSticker;
	Vector3 startposition;

	Transform finalParent;



	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		duplicateSticker = Instantiate (gameObject) as GameObject;
		duplicateSticker.transform.SetParent (UIPanelManager.Instance.hiddenPanel.transform);
		Destroy (duplicateSticker.GetComponent<DragHandler>());

		itemBeingDragged = duplicateSticker;
		startposition = gameObject.transform.position;
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		itemBeingDragged.transform.position = Input.mousePosition;
	}

	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{

		if (itemBeingDragged.transform.parent && itemBeingDragged.transform.parent.name != "Image") {
			itemBeingDragged.transform.position = startposition;
			Destroy (duplicateSticker);
		}
		itemBeingDragged = null;

		
	}

	#endregion
}
